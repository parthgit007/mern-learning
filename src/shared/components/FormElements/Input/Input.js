import React, { useReducer, useEffect } from "react";
import { validate } from "../../../utils/validators";

import "./Input.css";

const inputReducerFunction = (state, action) => {
    switch (action.type) {
        case "CHANGED":
            return {
                ...state,
                value: action.val,
                isValid: validate(action.val, action.validators),
            };
        case "TOUCH":
            return {
                ...state,
                isTouched: true,
            };
        default:
            return state;
    }
};

const Input = (props) => {
    const [inputValue, dispatch] = useReducer(inputReducerFunction, {
        value: props.value || '',
        isTouched: false,
        isValid: props.valid || false,
    });

    const {id, onInput} = props;
    const {value, isValid} = inputValue;

    useEffect(()=> {
        onInput(id, value, isValid);
    },[id, onInput, value, isValid])

    const onChangeHandler = (event) => {
        dispatch({
            type: "CHANGED",
            val: event.target.value,
            validators: props.validators,
        });
    };

    const onBlurHandler = () => {
        dispatch({
            type: "TOUCH",
        });
    };


    const element =
        props.element === "input" ? (
            <input
                id={props.id}
                type={props.type}
                placeholder={props.placeholder}
                onChange={onChangeHandler}
                value={inputValue.value}
                onBlur={onBlurHandler}
            />
        ) : (
            <textarea
                id={props.id}
                rows={props.rows || 3}
                onChange={onChangeHandler}
                value={inputValue.value}
                onBlur={onBlurHandler}

            />
        );
    return (
        <div
            className={`form-control ${!inputValue.isValid && inputValue.isTouched && 'form-control--invalid'}`}
        >
            <label htmlFor={props.id}>{props.label}</label>
            {element}
            {!inputValue.isValid && inputValue.isTouched && <p>{props.errorText}</p>}
        </div>
    );
};

export default Input;
