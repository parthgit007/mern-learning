import React, { useCallback, useReducer } from "react";
import Input from "../../shared/components/FormElements/Input/Input";
import {
    VALIDATOR_MINLENGTH,
    VALIDATOR_REQUIRE,
} from "../../shared/utils/validators";
import Button from "../../shared/components/FormElements/Button/Button";

import "./PlaceForm.css";
import { useForm } from "../../shared/hooks/form-hook";

const NewPlace = () => {
    const [formState, inputHandler] = useForm(
        {
            title: { value: "", isValid: false },
            description: { value: "", isValid: false },
            address: { value: "", isValid: false },
        },
        false
    );

    const newPlaceSubmitHandler = (event) => {
        event.preventDefault();
        console.log("formState", formState);
    };

    return (
        <form className='place-form' onSubmit={newPlaceSubmitHandler}>
            <Input
                id='title'
                type='text'
                label='Title'
                element='input'
                validators={[VALIDATOR_REQUIRE()]}
                errorText='Please enter valid title'
                onInput={inputHandler}
            />
            <Input
                id='description'
                type='textarea'
                label='description'
                element='textarea'
                validators={[VALIDATOR_MINLENGTH(5)]}
                errorText='Please enter valid description min to 5 row'
                onInput={inputHandler}
            />
            <Input
                id='address'
                type='text'
                label='Address'
                element='input'
                validators={[VALIDATOR_REQUIRE()]}
                errorText='Please enter valid address'
                onInput={inputHandler}
            />
            <Button type='submit' disabled={!formState.isValid}>
                Add Place
            </Button>
        </form>
    );
};

export default NewPlace;
