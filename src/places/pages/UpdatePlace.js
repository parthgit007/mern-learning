import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Button from "../../shared/components/FormElements/Button/Button";
import Input from "../../shared/components/FormElements/Input/Input";
import { useForm } from "../../shared/hooks/form-hook";
import {
    VALIDATOR_REQUIRE,
    VALIDATOR_MINLENGTH,
} from "../../shared/utils/validators";

import "./PlaceForm.css";
import Card from "../../shared/components/UIElements/Card";

const DUMMY_PLACES = [
    {
        id: "u1",
        title: "New York",
        description: "Worlds most popular city",
        imageUrl:
            "https://upload.wikimedia.org/wikipedia/commons/7/7a/View_of_Empire_State_Building_from_Rockefeller_Center_New_York_City_dllu_%28cropped%29.jpg",
        address: "20 W 34th St., New York, NY 10001, United States",
        location: {
            lat: 40.7484405,
            lng: -73.9878531,
        },
        creator: "u1",
    },
    {
        id: "u2",
        title: "Ontario CN Tower",
        description: "Worlds most popular city",
        imageUrl:
            "https://lh5.googleusercontent.com/p/AF1QipOvilsftYr2X6e-8z6waS9wB7vavakovMIO7hs=w408-h543-k-no",
        address: "290 Bremner Blvd, Toronto, ON M5V 3L9, Canada",
        location: {
            lat: 43.6425701,
            lng: -79.3892455,
        },
        creator: "u2",
    },
];

const UpdatePlace = () => {
    const placeId = useParams().placeId;
    const [formState, inputHandler, setFormData] = useForm(
        {
            title: { value: "", isValid: false },
            description: { value: "", isValid: false },
        },
        false
    );

    const identifyPlace = DUMMY_PLACES.find((item) => item.id === placeId);

    useEffect(() => {
        if (identifyPlace) {
            setFormData(
                {
                    title: { value: identifyPlace.title, isValid: true },
                    description: {
                        value: identifyPlace.description,
                        isValid: true,
                    },
                },
                true
            );
        }
    }, [setFormData, identifyPlace]);

    if (!identifyPlace) {
        return (
            <div className='center'>
                <Card>
                    <h2>No Place Found</h2>
                </Card>
            </div>
        );
    }

    const updatePlaceSubmitHandler = (event) => {
        event.preventDefault();
        console.log(formState.inputs);
    };

    if (formState.inputs.title.value === undefined) {
        return <p>Loading....</p>;
    }

    return (
        formState.inputs.title.value && (
            <form className='place-form' onSubmit={updatePlaceSubmitHandler}>
                <Input
                    id='title'
                    type='text'
                    element='input'
                    label='Title'
                    validators={[VALIDATOR_REQUIRE()]}
                    errorText='Please enter valid title'
                    value={formState.inputs.title.value}
                    onInput={inputHandler}
                    valid={formState.inputs.title.isValid}
                />
                <Input
                    id='description'
                    type='textarea'
                    element='textarea'
                    label='Description'
                    validators={[VALIDATOR_MINLENGTH(5)]}
                    errorText='Please enter valid description'
                    value={formState.inputs.description.value}
                    onInput={inputHandler}
                    valid={formState.inputs.description.isValid}
                />
                <Button type='submit' disabled={!formState.isValid}>
                    Update Place
                </Button>
            </form>
        )
    );
};

export default UpdatePlace;
