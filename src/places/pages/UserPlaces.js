import React from "react";
import {useParams} from "react-router-dom";

import PlaceList from "../components/PlaceList";

const DUMMY_PLACES = [
    {
        id: 'u1',
        title: "New York",
        description: "Worlds most popular city",
        imageUrl:
            "https://upload.wikimedia.org/wikipedia/commons/7/7a/View_of_Empire_State_Building_from_Rockefeller_Center_New_York_City_dllu_%28cropped%29.jpg",
        address: "20 W 34th St., New York, NY 10001, United States",
        location: {
            lat: 40.7484405,
            lng: -73.9878531,
        },
        creator: "u1",
    },
    {
        id: 'u2',
        title: "Ontario CN Tower",
        description: "Worlds most popular city",
        imageUrl:
            "https://lh5.googleusercontent.com/p/AF1QipOvilsftYr2X6e-8z6waS9wB7vavakovMIO7hs=w408-h543-k-no",
        address: "290 Bremner Blvd, Toronto, ON M5V 3L9, Canada",
        location: {
            lat: 43.6425701,
            lng: -79.3892455,
        },
        creator: "u2",
    },
];

function UserPlaces(props) {
    const routeParams = useParams();
    const placeList = DUMMY_PLACES.filter(item => item.creator === routeParams.userId);
    console.log(routeParams);
    return <PlaceList items={placeList} />;
}

export default UserPlaces;
